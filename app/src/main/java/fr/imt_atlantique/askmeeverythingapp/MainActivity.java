package fr.imt_atlantique.askmeeverythingapp;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    private final int SNACKBAR_DURATION = 2000;
    private final String DEFAULT_USER_QUESTION = "Aucune question. (répondez ce que vous voulez)";

    private EditText questionInput;
    private Button sendQuestionBtn;
    private String userQuestion;
    private View mainView;

    private ActivityResultLauncher mGetUserAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        this.userQuestion = DEFAULT_USER_QUESTION;

        this.mGetUserAnswer = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                String msg = result.getData().getStringExtra(String.valueOf(R.string.extra_user_answer));
                Snackbar.make(mainView, msg, SNACKBAR_DURATION).show();
            }
        });
    }

    private void initViews() {
        mainView = findViewById(R.id.main_view);
        questionInput = (EditText) findViewById(R.id.question_input);
        sendQuestionBtn = (Button) findViewById(R.id.send_question_btn);

        sendQuestionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userQuestion = questionInput.getText().toString();
                Intent answerIntent = new Intent(getBaseContext(), AnswerQuestionActivity.class);
                answerIntent.putExtra(String.valueOf(R.string.extra_user_question), userQuestion);
                mGetUserAnswer.launch(answerIntent);
            }
        });
    }
}