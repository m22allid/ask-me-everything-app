package fr.imt_atlantique.askmeeverythingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AnswerQuestionActivity extends AppCompatActivity {

    private final int RESPONSE_CODE_OK = 0;
    // private final String EXTRA_USER_QUESTION = "USER_QUESTION";
    // private final String EXTRA_USER_ANSWER = "USER_ANSWER";

    private EditText answerInput;
    private Button sendAnswerBtn;
    private TextView userQuestionView;

    private String userAnswer;
    private String userQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_question);

        initViews();

        userAnswer = "";
        userQuestion = getIntent().getStringExtra(String.valueOf(R.string.extra_user_question));

        setUserQuestion(userQuestion);
    }

    private void initViews() {
        answerInput = (EditText) findViewById(R.id.answer_input);
        sendAnswerBtn = (Button) findViewById(R.id.send_answer_btn);
        userQuestionView = (TextView) findViewById(R.id.question_textview);

        sendAnswerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserAnswer();
                Intent answerIntent = new Intent(getBaseContext(), MainActivity.class);
                answerIntent.putExtra(String.valueOf(R.string.extra_user_answer), userAnswer);
                setResult(RESPONSE_CODE_OK, answerIntent);
                finish();
            }
        });
    }

    private void setUserQuestion(String text) {
        userQuestionView.setText(text);
    }

    private String getUserAnswer() {
        userAnswer = answerInput.getText().toString();

        return userAnswer;
    }
}